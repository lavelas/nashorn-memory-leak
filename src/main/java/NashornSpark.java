import spark.servlet.SparkApplication;

import javax.script.*;

/**
 * Spark filter with demonstration memory leak in Nashorn
 */
public class NashornSpark implements SparkApplication {

    public void init() {
        ScriptEngineManager sem = new ScriptEngineManager();
        ScriptEngine nashorn = sem.getEngineByName("nashorn");

        try {
            nashorn.eval(
                    "var spark = {};\n" +
                    "spark.before = Packages.spark.Spark.before;\n" +
                    "spark.get = Packages.spark.Spark.get;\n" +
                    "spark.before('/*', function (request, response) { print('before') });\n" + // <------ Comment this line FIX memory leak!!!
                    "spark.get('/*', function (request, response) { return 'Hello spark!' });" +
                    ""
            );
        } catch (ScriptException e) {
            e.printStackTrace();
        }
    }
}
